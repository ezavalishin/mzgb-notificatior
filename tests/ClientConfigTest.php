<?php

namespace DigitalDev\MzgbNotificator\Tests;

use DigitalDev\MzgbNotificator\ClientConfig;
use DigitalDev\MzgbNotificator\QueryPreparers\DefaultQueryPreparer;
use DigitalDev\MzgbNotificator\Tests\Helpers\BarMiddleware;
use DigitalDev\MzgbNotificator\Tests\Helpers\BarOnceMiddleware;
use DigitalDev\MzgbNotificator\Tests\Helpers\FooMiddleware;
use DigitalDev\MzgbNotificator\Tests\Helpers\FooOnceMiddleware;
use PHPUnit\Framework\TestCase;

class ClientConfigTest extends TestCase
{
    /**
     * @covers \DigitalDev\MzgbNotificator\ClientConfig::__construct
     */
    public function testConstructConfigurationMismatch(): void
    {
        $this->expectException(\RuntimeException::class);
        new ClientConfig('clientName', 'serviceName', []);
    }

    /**
     * @covers \DigitalDev\MzgbNotificator\ClientConfig::__construct
     */
    public function testConstructFull(): void
    {
        $data = [
            'url'           => 'http://test.com/jsonrpc',
            'clientClass'   => 'MyStubClass',
            'middleware'    => [
                FooMiddleware::class,
                FooOnceMiddleware::class => [
                    'foo'   => 'bar',
                    'hello' => 'world',
                ],
                BarMiddleware::class     => [
                    'foo'   => 'bar',
                    'hello' => 'world',
                ],
                BarOnceMiddleware::class,
            ],
            'queryPreparer' => 'TestQueryPreparer',
            'extendedStubs' => true,
        ];

        $middlewareConfigured = [
            [FooMiddleware::class, []],
            [
                BarMiddleware::class,
                [
                    'foo'   => 'bar',
                    'hello' => 'world',
                ],
            ],
        ];

        $middlewareOnceConfigured = [
            [
                FooOnceMiddleware::class,
                [
                    'foo'   => 'bar',
                    'hello' => 'world',
                ],
            ],
            [BarOnceMiddleware::class, []],
        ];

        $instance = new ClientConfig('clientName', 'serviceName', $data);

        $this->assertEquals('clientName', $instance->clientName);
        $this->assertEquals('serviceName', $instance->serviceName);
        $this->assertEquals($data['url'], $instance->url);
        $this->assertEquals($data['clientClass'], $instance->clientClass);
        $this->assertEquals($middlewareConfigured, $instance->middleware);
        $this->assertEquals($middlewareOnceConfigured, $instance->onceExecutedMiddleware);
        $this->assertEquals($data['queryPreparer'], $instance->queryPreparer);
        $this->assertEquals($data['extendedStubs'], $instance->extendedStubs);
    }

    /**
     * @covers \DigitalDev\MzgbNotificator\ClientConfig::__construct
     */
    public function testConstructDefault(): void
    {
        $data = [
            'url'         => 'http://test.com/jsonrpc',
            'clientClass' => 'MyStubClass',
        ];

        $config = new ClientConfig('clientName', 'serviceName', $data);

        $this->assertEquals('clientName', $config->clientName);
        $this->assertEquals('serviceName', $config->serviceName);
        $this->assertEquals($data['url'], $config->url);
        $this->assertEquals($data['clientClass'], $config->clientClass);
        $this->assertEquals([], $config->middleware);
        $this->assertEquals(DefaultQueryPreparer::class, $config->queryPreparer);
        $this->assertEquals(false, $config->extendedStubs);
    }
}
