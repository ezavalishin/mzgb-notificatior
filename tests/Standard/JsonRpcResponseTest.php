<?php

namespace DigitalDev\MzgbNotificator\Tests\Standard;

use DigitalDev\MzgbNotificator\Standard\JsonRpcError;
use DigitalDev\MzgbNotificator\Standard\JsonRpcResponse;
use PHPUnit\Framework\TestCase;

class JsonRpcResponseTest extends TestCase
{
    /**
     * @covers \DigitalDev\MzgbNotificator\Standard\JsonRpcResponse::__construct
     */
    public function testConstructResult(): void
    {
        $data = (object) [
            'jsonrpc' => '2.0',
            'id'      => 123,
            'result'  => 'SomeResult',
        ];

        $instance = new JsonRpcResponse($data);
        $this->assertEquals($data->jsonrpc, $instance->jsonrpc);
        $this->assertEquals($data->id, $instance->id);
        $this->assertEquals($data->result, $instance->result);
        $this->assertNull($instance->error);
    }

    /**
     * @covers \DigitalDev\MzgbNotificator\Standard\JsonRpcResponse::__construct
     */
    public function testConstructError(): void
    {
        $data = (object) [
            'jsonrpc' => '2.0',
            'id'      => 123,
            'error'   => (object) [
                'code'    => 123,
                'message' => 'Some message',
            ],
        ];

        $instance = new JsonRpcResponse($data);
        $this->assertEquals($data->jsonrpc, $instance->jsonrpc);
        $this->assertEquals($data->id, $instance->id);
        $this->assertInstanceOf(JsonRpcError::class, $instance->error);
        $this->assertEquals($data->error->code, $instance->error->code);
        $this->assertEquals($data->error->message, $instance->error->message);
        $this->assertNull($instance->result);
    }
}
