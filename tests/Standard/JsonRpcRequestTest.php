<?php

namespace DigitalDev\MzgbNotificator\Tests\Standard;

use DigitalDev\MzgbNotificator\Standard\JsonRpcRequest;
use PHPUnit\Framework\TestCase;

class JsonRpcRequestTest extends TestCase
{
    /**
     * @covers \DigitalDev\MzgbNotificator\Standard\JsonRpcRequest::toArray
     */
    public function testToArray(): void
    {
        $params = ['foo' => 'bar', 'hello' => 'world'];
        $instance = new JsonRpcRequest('test', $params, 123);

        $result = $instance->toArray();

        $this->assertEquals('test', $result['method']);
        $this->assertEquals('2.0', $result['jsonrpc']);
        $this->assertEquals($params, $result['params']);
        $this->assertEquals(123, $result['id']);
    }
}
