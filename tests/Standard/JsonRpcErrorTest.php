<?php

namespace DigitalDev\MzgbNotificator\Tests\Standard;

use DigitalDev\MzgbNotificator\Standard\JsonRpcError;
use PHPUnit\Framework\TestCase;

class JsonRpcErrorTest extends TestCase
{
    /**
     * @covers \DigitalDev\MzgbNotificator\Standard\JsonRpcError::__construct
     */
    public function testConstructFull(): void
    {
        $data = (object) [
            'code'    => 123,
            'message' => 'Test message',
            'data'    => (object) [
                'foo'   => 'bar',
                'hello' => 'world',
            ],
        ];
        $instance = new JsonRpcError($data);

        $this->assertEquals($data->code, $instance->code);
        $this->assertEquals($data->message, $instance->message);
        $this->assertEquals($data->data, $instance->data);
    }

    /**
     * @covers \DigitalDev\MzgbNotificator\Standard\JsonRpcError::__construct
     */
    public function testConstructDefault(): void
    {
        $data = (object) [
            'code'    => 123,
            'message' => 'Test message',
        ];
        $instance = new JsonRpcError($data);

        $this->assertEquals($data->code, $instance->code);
        $this->assertEquals($data->message, $instance->message);
        $this->assertInstanceOf(\stdClass::class, $instance->data);
    }
}
