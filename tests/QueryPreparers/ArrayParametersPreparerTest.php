<?php

namespace DigitalDev\MzgbNotificator\Tests\QueryPreparers;

use DigitalDev\MzgbNotificator\ClientConfig;
use DigitalDev\MzgbNotificator\QueryPreparers\ArrayParametersPreparer;
use PHPUnit\Framework\TestCase;

class ArrayParametersPreparerTest extends TestCase
{
    /**
     * @covers \DigitalDev\MzgbNotificator\QueryPreparers\ArrayParametersPreparer::prepare
     */
    public function testPrepare()
    {
        $clientConfig = new ClientConfig('clientName', 'serviceName', ['url' => 'url', 'clientClass' => 'clientClass']);
        $preparer = new ArrayParametersPreparer();
        $jsonRpcRequest = $preparer->prepare('method', ['param1' => 'value1', 'param2' => 'value2'], $clientConfig);
        $this->assertNotNull($jsonRpcRequest->id);
        $this->assertSame('method', $jsonRpcRequest->method);
        $this->assertSame(['value1', 'value2'], $jsonRpcRequest->params);

    }
}
