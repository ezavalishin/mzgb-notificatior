<?php

namespace DigitalDev\MzgbNotificator\Tests;

use DigitalDev\MzgbNotificator\Result;
use PHPUnit\Framework\TestCase;

class EmptyResultTest extends TestCase
{
    /**
     * @covers \DigitalDev\MzgbNotificator\Result::__toString
     */
    public function testToString(): void
    {
        $instance = new Result();

        $this->assertEquals('The response has not yet been initialized', (string) $instance);
    }
}
