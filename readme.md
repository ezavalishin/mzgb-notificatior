# Mzgb Notificator

## Установка

``` composer require digital-dev/mzgb-notificatior ```

## Настройки

Опубликовать файл конфигурации

``` php artisan vendor:publish --tag="mzgb-config" ```

Запонлить url (MZGB_NOTIFICATOR_URL) и секретный ключ доступа (MZGB_NOTIFICATOR_ACCESS_KEY)

## Использование

Возможно использование через фасад и DI

```php
use \DigitalDev\MzgbNotificator\Facades\MzgbHelper;

MzgbHelper::getTgLink(int $userId) : string // возвращает ссылку регистрации в системе для конкретного пользователя в телеграме
MzgbHelper::getVkLink(int $userId) : string // возвращает ссылку регистрации в системе для конкретного пользователя в вк
MzgbHelper::sendNotification(int $userId, string $message, array $buttons = null) : object // отправляет уведомление конкретному пользователю. Подробнее о параметрах ниже
MzgbHelper::sendNotificationToAll(array $userIds, string $message, array $buttons = null) // отправляет уведомления массиву пользователей. Подробнее о параметрах ниже
```

## Notification channel

Доступна отправка уведомлений через функционал нотификаций. Для этого необходимо указать канал ``` DigitalDev\MzgbNotificator\Channels\MzgbChannel ```

А так же в самом классе уведмоления добавить метод ``` public function toMzgb($notifiable) : DigitalDev\MzgbNotificator\Keyboard\Message ```

Пример:

```php
<?php
namespace App\Notifications;

use DigitalDev\MzgbNotificator\Channels\MzgbChannel;
use DigitalDev\MzgbNotificator\Keyboard\Button;
use DigitalDev\MzgbNotificator\Keyboard\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class TestNotification extends Notification
{
    use Queueable;

    public function via($notifiable)
    {
        return [MzgbChannel::class];
    }

    public function toMzgb($notifiable)
    {
        $message = new Message('test notification');
        $message->setKeyboard([
            (new Button('test button url'))->setCallbackUrl('https://ya.ru'),
            (new Button('test button message'))->setCallbackMessage('response message'),
        ]);

        return $message;
    }
}

```

## Вспомогательные классы

```php
/**
 * Вспомогательный класс соббщения для использования в классе Уведомления
* @method setKeyboard(array $buttons) Метод для установки клавиатуры. Принимает масси Button
 */
new \DigitalDev\MzgbNotificator\Keyboard\Message(string $message)


/**
 * Вспомогательный класс для кнопки в клавиатуре бота
* @method setCallbackMessage(string $message) Метод для установки обработчика кнопки. При нажатии на кнопку, пользователю в ответ придет это сообщение
* @method setCallbackUrl(string $url) Метод для установки обработчика кнопки. При нажатии на кнопку, произойдет запрос на данный урл. Подробнее ниже
 */
new \DigitalDev\MzgbNotificator\Keyboard\Button(string $label)
```

## Установка callback-урла для кнопки

Данная схема позволяет выполнить запрос на указанный в кнопке URL после нажатия кнопки пользователем (Например: можно подтверждать регистрацию на игру)

Сервер делает POST запрос на указанный урл и ожидает код 200 и json следующего вида:

```json
{
  "message": "Сообщение, которое будет отправлено пользователю в ответ на нажатие"
}
```

Если нет поля ``` message ``` в отвер придет стандратное сообщение об успехе

Если данный урл недоступен, пользователю в ответ придет сообщение об ошибке и просьба повторить позже 
