<?php

namespace DigitalDev\MzgbNotificator;

use DigitalDev\MzgbNotificator\Contracts\QueryPreparer;
use DigitalDev\MzgbNotificator\Contracts\TransportClient;
use DigitalDev\MzgbNotificator\Exceptions\JsonRpcClientException;
use DigitalDev\MzgbNotificator\Exceptions\ResponseException;
use DigitalDev\MzgbNotificator\Middleware\MiddlewarePipeline;
use DigitalDev\MzgbNotificator\Standard\JsonRpcRequest;
use Exception;
use Illuminate\Container\Container;
use function count;

/**
 * Class Client
 *
 * @package Tochka\JsonRpcClient
 * @method static static batch()
 * @method static static cache($minutes = -1)
 * @method static static with(string $name, $value)
 * @method static static withValues(array $values)
 * @method static execute()
 * @method static mixed call(string $method, array $params)
 * @method static Response|mixed notification_send(mixed $userId, mixed $message, mixed $keyboard = null)
 * @method static Response|mixed notification_sendToAll(mixed $userIds, mixed $message)
 * @method static Response|mixed user_getLinks(mixed $userId)
 */
class Client
{
    /** @var ClientConfig */
    protected $config;
    /** @var bool */
    protected $executeImmediately = true;

    /** @var Request[] */
    protected $requests = [];
    /** @var array */
    protected $results = [];

    /** @var array */
    protected $additionalValues = [];
    /** @var QueryPreparer */
    protected $queryPreparer;
    /** @var TransportClient */
    protected $transportClient;

    public function __construct(ClientConfig $config, QueryPreparer $queryPreparer, TransportClient $client)
    {
        $this->reset();

        $this->config = $config;
        $this->queryPreparer = $queryPreparer;
        $this->transportClient = $client;
    }

    /**
     * @param $method
     * @param $params
     *
     * @return mixed
     * @throws Exception
     */
    public function __call($method, $params)
    {
        if (method_exists($this, '_' . $method)) {
            return $this->{'_' . $method}(...$params);
        }

        return $this->_call($method, $params);
    }

    /**
     * Помечает экземпляр клиента как массив вызовов
     *
     * @return $this
     */
    protected function _batch(): self
    {
        $this->reset();
        $this->executeImmediately = false;

        return $this;
    }

    /**
     * @param string $name
     * @param mixed $value
     *
     * @return Client
     */
    protected function _with(string $name, $value): self
    {
        $this->additionalValues[$name] = $value;

        return $this;
    }

    /**
     * @param array $values
     *
     * @return Client
     */
    protected function _withValues(array $values): self
    {
        $this->additionalValues = array_merge($this->additionalValues, $values);

        return $this;
    }

    /**
     * Помечает вызываемый метод кешируемым
     *
     * @param int $minutes
     *
     * @return $this
     */
    protected function _cache($minutes = -1): self
    {
        $this->additionalValues['cache'] = $minutes;

        return $this;
    }

    /** @noinspection MagicMethodsValidityInspection */
    /**
     * Выполняет удаленный вызов (либо добавляет его в массив)
     *
     * @param string $method
     * @param array  $params
     *
     * @return mixed
     * @throws Exception
     */
    protected function _call($method, $params)
    {
        $jsonRpcRequest = $this->queryPreparer->prepare($method, $params, $this->config);
        $request = new Request($jsonRpcRequest);
        $request->setAdditional($this->additionalValues);

        $this->additionalValues = [];

        $this->requests[$request->getId()] = $request;
        $this->results[$request->getId()] = $this->requests[$request->getId()]->getResult();

        if ($this->executeImmediately) {
            $result = $this->_execute();
            if (count($result) > 0) {
                return $result[0];
            }
        }

        return $this->results[$request->getId()];
    }

    /**
     * Выполняет запрос всех вызовов
     *
     * @return array
     * @throws Exception
     */
    protected function _execute(): array
    {
        $requests = $this->handleMiddleware();

        if (!count($requests)) {
            return [];
        }

        $responses = $this->transportClient->get($requests, $this->config);

        try {
            foreach ($responses as $response) {
                if (isset($this->requests[$response->id])) {
                    $this->requests[$response->id]->setJsonRpcResponse($response);
                    $this->results[$response->id] = $this->requests[$response->id]->getResult();
                } else {
                    if (!empty($response->error)) {
                        throw new ResponseException($response->error);
                    }

                    throw new JsonRpcClientException(0, 'Unknown response');
                }
            }

            $results = $this->results;
        } catch (Exception $e) {
            throw $e;
        } finally {
            $this->reset();
        }

        return array_values(array_map(static function (Result $item) {
            return $item->get();
        }, $results));
    }

    /**
     * @return JsonRpcRequest[]
     */
    protected function handleMiddleware(): array
    {
        $pipeline = new MiddlewarePipeline(Container::getInstance());
        $pipeline->setAdditionalDIInstances($this->config, $this->transportClient);

        $requests = [];
        foreach ($this->requests as $request) {
            $request = $pipeline->send($request)
                ->through($this->config->middleware)
                ->via('handle')
                ->then(static function (Request $request) {
                    return ($request->getResult() instanceof Result)
                        ? $request->getJsonRpcRequest()
                        : null;
                });

            if ($request) {
                $requests[] = $request;
            }
        }

        $requests = $pipeline->send($requests)
            ->through($this->config->onceExecutedMiddleware)
            ->via('handle')
            ->then(static function (array $requests) {
                return $requests;
            });

        return $requests;
    }

    protected function reset(): void
    {
        $this->executeImmediately = true;
        $this->requests = [];
        $this->additionalValues = [];
        $this->results = [];
    }
}
