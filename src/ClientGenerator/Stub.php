<?php

namespace DigitalDev\MzgbNotificator\ClientGenerator;

interface Stub
{
    public function getClassSource();

    public function __toString();
}
