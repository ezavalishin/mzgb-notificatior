<?php

namespace DigitalDev\MzgbNotificator\Keyboard;

class Message
{
    protected $message;
    protected $keyboard;

    public function __construct($message, array $keyboard = [])
    {
        $this->message = $message;
        $this->keyboard = $keyboard;
    }

    public function setKeyboard(array $buttons): Message
    {
        $this->keyboard = $buttons;
        return $this;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getButtons(): array
    {
        return $this->keyboard;
    }
}
