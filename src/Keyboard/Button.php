<?php

namespace DigitalDev\MzgbNotificator\Keyboard;

class Button
{
    protected $label;
    protected $callbackMessage;
    protected $callbackUrl;

    public function __construct($label)
    {
        $this->label = $label;
    }

    public function setCallbackMessage($message): Button
    {
        $this->callbackMessage = $message;
        return $this;
    }

    public function setCallbackUrl($url): Button
    {
        $this->callbackUrl = $url;
        return $this;
    }

    public function toArray(): array
    {
        $result = [
            'label' => $this->label
        ];
        if ($this->callbackMessage) {
            $result['callback_message'] = $this->callbackMessage;
        }

        if ($this->callbackUrl) {
            $result['callback_url'] = $this->callbackUrl;
        }

        return $result;
    }
}
