<?php

namespace DigitalDev\MzgbNotificator\Keyboard;

use Illuminate\Support\Collection;

class Keyboard {

    private $buttons;

    /**
     * Keyboard constructor.
     * @param Button[] $buttons
     */
    public function __construct(array $buttons = null)
    {
        $this->buttons = $buttons;
    }

    public function toArray(): array
    {
        return (new Collection($this->buttons))->map(static function(Button $button) {
           return $button->toArray();
        })->toArray();
    }
}
