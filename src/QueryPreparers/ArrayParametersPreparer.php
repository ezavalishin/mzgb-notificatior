<?php

namespace DigitalDev\MzgbNotificator\QueryPreparers;

use DigitalDev\MzgbNotificator\ClientConfig;
use DigitalDev\MzgbNotificator\Contracts\QueryPreparer;
use DigitalDev\MzgbNotificator\Standard\JsonRpcRequest;

class ArrayParametersPreparer implements QueryPreparer
{
    /**
     * @param string                             $method
     * @param array                              $params
     * @param \DigitalDev\MzgbNotificator\ClientConfig $config
     *
     * @return \DigitalDev\MzgbNotificator\Standard\JsonRpcRequest
     */
    public function prepare(string $method, array $params, ClientConfig $config): JsonRpcRequest
    {
        $id = uniqid($config->clientName, true);

        return new JsonRpcRequest($method, array_values($params), $id);
    }
}
