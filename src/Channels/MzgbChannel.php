<?php

namespace DigitalDev\MzgbNotificator\Channels;

use DigitalDev\MzgbNotificator\Keyboard\Message;
use DigitalDev\MzgbNotificator\MzgbHelper;
use Illuminate\Notifications\Notification;

class MzgbChannel
{
    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        /**
         * @var Message $message
         */
        $message = $notification->toMzgb($notifiable);

        \DigitalDev\MzgbNotificator\Facades\MzgbHelper::sendNotification($notifiable->id, $message->getMessage(), $message->getButtons());
    }
}
