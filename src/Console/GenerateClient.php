<?php /** @noinspection ClassOverridesFieldOfSuperClassInspection */

namespace DigitalDev\MzgbNotificator\Console;

use DigitalDev\MzgbNotificator\ClientConfig;
use DigitalDev\MzgbNotificator\ClientGenerator\ClientGenerator;
use Illuminate\Console\Command;

class GenerateClient extends Command
{
    protected $signature = 'jsonrpc:generateClient {connection?}';

    protected $description = 'Generate proxy-client for JsonRpc server by SMD-scheme';

    /**
     * @throws \DigitalDev\MzgbNotificator\Exceptions\JsonRpcClientException
     */
    public function handle(): void
    {
        $connection = $this->argument('connection');

        if ($connection === null) {
            $connections = config('jsonrpc-client.connections');
            foreach ($connections as $key => $connection) {
                $this->generate($key);
            }
        } else {
            $this->generate($connection);
        }
    }

    /**
     * @param string $connection
     *
     * @throws \DigitalDev\MzgbNotificator\Exceptions\JsonRpcClientException
     */
    protected function generate(string $connection): void
    {
        $this->info('Generate client class for connection: ' . $connection);

        $services = config('jsonrpc-client.connections', []);
        $clientName = config('jsonrpc-client.clientName', []);
        $config = new ClientConfig($clientName, $connection, $services[$connection] ?? []);

        (new ClientGenerator($config))->generate();
        $this->info('Success!');
    }
}
