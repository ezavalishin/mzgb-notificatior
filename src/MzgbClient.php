<?php

namespace DigitalDev\MzgbNotificator;

use Illuminate\Support\Facades\Facade;

/**
 * @author JsonRpcClientGenerator
 * @date 13.02.2020 12:30
 * @mixin Client
 * @method static object notification_send(int $userId, string $message, array|null $keyboard = null)
 * @method static object notification_sendToAll(array $userIds, string $message, array|null $keyboard = null)
 * @method static object user_getLinks(int $userId)
 *
 */
class MzgbClient extends Facade
{
    public static function getFacadeAccessor(): string
    {
        return self::class;
    }
}
