<?php

namespace DigitalDev\MzgbNotificator\Contracts;

use DigitalDev\MzgbNotificator\ClientConfig;
use DigitalDev\MzgbNotificator\Standard\JsonRpcRequest;

interface QueryPreparer
{
    public function prepare(string $method, array $params, ClientConfig $config): JsonRpcRequest;
}
