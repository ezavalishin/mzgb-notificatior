<?php

namespace DigitalDev\MzgbNotificator\Contracts;

use DigitalDev\MzgbNotificator\ClientConfig;

interface TransportClient
{
    /**
     * @param \DigitalDev\MzgbNotificator\Standard\JsonRpcRequest[] $request
     * @param \DigitalDev\MzgbNotificator\ClientConfig              $config
     *
     * @return \DigitalDev\MzgbNotificator\Standard\JsonRpcResponse[]
     */
    public function get(array $request, ClientConfig $config): array;
}
