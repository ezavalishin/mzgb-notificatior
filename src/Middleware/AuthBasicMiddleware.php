<?php

namespace DigitalDev\MzgbNotificator\Middleware;

use DigitalDev\MzgbNotificator\Client\HttpClient;
use DigitalDev\MzgbNotificator\Contracts\OnceExecutedMiddleware;
use DigitalDev\MzgbNotificator\Contracts\TransportClient;
use DigitalDev\MzgbNotificator\Standard\JsonRpcRequest;
use GuzzleHttp\RequestOptions;

class AuthBasicMiddleware implements OnceExecutedMiddleware
{
    /**
     * @param JsonRpcRequest[] $requests
     * @param \Closure         $next
     * @param TransportClient  $client
     * @param string           $username
     * @param string           $password
     * @param string           $scheme
     *
     * @return mixed
     */
    public function handle(
        array $requests,
        \Closure $next,
        TransportClient $client,
        $username = '',
        $password = '',
        $scheme = 'basic'
    ) {
        if (!$client instanceof HttpClient) {
            return $next($requests);
        }

        $client->setOption(RequestOptions::AUTH, [
            $username,
            $password,
            $scheme,
        ]);

        return $next($requests);
    }
}
