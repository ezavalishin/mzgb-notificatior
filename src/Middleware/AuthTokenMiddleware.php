<?php

namespace DigitalDev\MzgbNotificator\Middleware;

use DigitalDev\MzgbNotificator\Client\HttpClient;
use DigitalDev\MzgbNotificator\Contracts\OnceExecutedMiddleware;
use DigitalDev\MzgbNotificator\Contracts\TransportClient;
use DigitalDev\MzgbNotificator\Standard\JsonRpcRequest;

class AuthTokenMiddleware implements OnceExecutedMiddleware
{
    /**
     * @param JsonRpcRequest[] $requests
     * @param \Closure         $next
     * @param TransportClient  $client
     * @param string           $value
     * @param string           $name
     *
     * @return mixed
     */
    public function handle(
        array $requests,
        \Closure $next,
        TransportClient $client,
        $value,
        $name = 'X-Access-Key'
    ) {
        if (!$client instanceof HttpClient) {
            return $next($requests);
        }

        $client->setHeader($name, $value);

        return $next($requests);
    }
}
