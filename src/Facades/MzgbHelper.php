<?php

namespace DigitalDev\MzgbNotificator\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class MzgbHelper
 * @method static string getVkLink(int $userId)
 * @method static string getTgLink(int $userId)
 * @method static object sendNotification(int $userId, string $message, array $buttons = null)
 * @method static object sendNotificationToAll(array $userIds, string $message, array $buttons = null)
 */
class MzgbHelper extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \DigitalDev\MzgbNotificator\MzgbHelper::class;
    }
}
