<?php

namespace DigitalDev\MzgbNotificator;

use DigitalDev\MzgbNotificator\Keyboard\Button;

class MzgbHelper
{
    private static $links;

    private $client;

    public function __construct($client)
    {
        $this->client = $client;
    }

    public function getLinks(int $userId)
    {
        if (!isset(self::$links[$userId])) {
            self::$links[$userId] = $this->client->user_getLinks($userId)->links;
        }

        return self::$links[$userId];
    }

    public function getVkLink(int $userId)
    {
        return $this->getLinks($userId)->vk;
    }

    public function getTgLink(int $userId)
    {
        return $this->getLinks($userId)->tg;
    }

    /**
     * @param int $userId
     * @param string $message
     * @param Button[] $buttons
     * @return object
     */
    public function sendNotification(int $userId, string $message, array $buttons = null)
    {
        $keyboard = new Keyboard\Keyboard($buttons);
        return $this->client->notification_send($userId, $message, $keyboard->toArray());
    }

    public function sendNotificationToAll(array $userIds, string $message, array $buttons = null)
    {
        $keyboard = new Keyboard\Keyboard($buttons);
        return $this->client->notification_sendToAll($userIds, $message, $keyboard->toArray());
    }
}
