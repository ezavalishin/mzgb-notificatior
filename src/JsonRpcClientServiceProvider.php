<?php

namespace DigitalDev\MzgbNotificator;

use DigitalDev\MzgbNotificator\Client\HttpClient;
use DigitalDev\MzgbNotificator\Console\GenerateClient;
use Illuminate\Support\ServiceProvider;

class JsonRpcClientServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateClient::class,
            ]);
        }

        $this->publishes([
            __DIR__ . '/../config/mzgb-notifications.php' => base_path('config/mzgb-notifications.php'),
        ], 'mzgb-config');
    }

    public function register()
    {

        $this->registerMzgb();
    }

    public function registerMzgb()
    {

        $clientName = config('mzgb-notifications.clientName');
        $alias = 'default';
        $serviceConfig = [
            'url' => config('mzgb-notifications.url'),
            'clientClass' => MzgbClient::class,
            'extendedStubs' => false,
            'middleware' => config('mzgb-notifications.middleware'),
            'namedParameters' => true
        ];

        $this->app->singleton(MzgbHelper::class, function () use ($clientName, $alias, $serviceConfig) {

            $config = new ClientConfig($clientName, $alias, $serviceConfig);
            $client = new HttpClient();
            $queryPreparer = $this->app->get($config->queryPreparer);

            return new MzgbHelper(new Client($config, $queryPreparer, $client));
        });
    }
}
