<?php

namespace DigitalDev\MzgbNotificator\Types;

class BaseObjectClass implements ParameterValue
{
    public function getValue()
    {
        return (object)$this;
    }
}
