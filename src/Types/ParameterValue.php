<?php

namespace DigitalDev\MzgbNotificator\Types;

interface ParameterValue
{
    public function getValue();
}
