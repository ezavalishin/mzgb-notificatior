<?php

namespace DigitalDev\MzgbNotificator;

use DigitalDev\MzgbNotificator\Exceptions\ResponseException;
use DigitalDev\MzgbNotificator\Standard\JsonRpcRequest;
use DigitalDev\MzgbNotificator\Standard\JsonRpcResponse;

class Request
{
    /** @var \DigitalDev\MzgbNotificator\Standard\JsonRpcRequest */
    protected $jsonRpcRequest;
    /** @var \DigitalDev\MzgbNotificator\Standard\JsonRpcResponse */
    protected $jsonRpcResponse;

    protected $result;
    protected $additional = [];

    public function __construct(JsonRpcRequest $request)
    {
        $this->jsonRpcRequest = $request;
        $this->result = new Result();
    }

    /**
     * @param array $values
     *
     * @codeCoverageIgnore
     */
    public function setAdditional(array $values): void
    {
        $this->additional = $values;
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function getAdditional($key, $default = null)
    {
        return $this->additional[$key] ?? $default;
    }

    /**
     * @return Result
     * @codeCoverageIgnore
     */
    public function getResult(): Result
    {
        return $this->result;
    }

    /**
     * @return string
     * @codeCoverageIgnore
     */
    public function getId(): string
    {
        return $this->jsonRpcRequest->id;
    }

    /**
     * @return \DigitalDev\MzgbNotificator\Standard\JsonRpcRequest
     * @codeCoverageIgnore
     */
    public function getJsonRpcRequest(): JsonRpcRequest
    {
        return $this->jsonRpcRequest;
    }

    /**
     * @param \DigitalDev\MzgbNotificator\Standard\JsonRpcRequest $request
     *
     * @codeCoverageIgnore
     */
    public function setJsonRpcRequest(JsonRpcRequest $request): void
    {
        $this->jsonRpcRequest = $request;
    }

    /**
     * @return \DigitalDev\MzgbNotificator\Standard\JsonRpcResponse
     * @codeCoverageIgnore
     */
    public function getJsonRpcResponse(): JsonRpcResponse
    {
        return $this->jsonRpcResponse;
    }

    /**
     * @param \DigitalDev\MzgbNotificator\Standard\JsonRpcResponse $response
     *
     * @throws \DigitalDev\MzgbNotificator\Exceptions\ResponseException
     */
    public function setJsonRpcResponse(JsonRpcResponse $response): void
    {
        $this->jsonRpcResponse = $response;
        $this->parseResult();
    }

    /**
     * @throws \DigitalDev\MzgbNotificator\Exceptions\ResponseException
     */
    protected function parseResult(): void
    {
        if (!empty($this->jsonRpcResponse->error)) {
            throw new ResponseException($this->jsonRpcResponse->error);
        }

        $this->result->setResult($this->jsonRpcResponse->result);
    }
}
