<?php

return [
    'clientName' => 'default',
    'url' => env('MZGB_NOTIFICATOR_URL', 'https://api.jsonrpc.com/v1/jsonrpc'),

    'middleware'      => [
//                \Tochka\JsonRpcClient\Middleware\AdditionalHeadersMiddleware::class => [
//                    'headerName1' => 'headerValue',
//                    'headerName2' => ['value1', 'value2',], // To include multiple headers with the same name
//                ],

        \DigitalDev\MzgbNotificator\Middleware\AuthTokenMiddleware::class => [
            'name'  => 'X-Access-Key',
            'value' => env('MZGB_NOTIFICATOR_ACCESS_KEY', 'TOKEN'),
        ]
    ],
];
